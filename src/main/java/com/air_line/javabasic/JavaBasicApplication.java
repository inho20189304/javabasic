package com.air_line.javabasic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaBasicApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaBasicApplication.class, args);

        boolean myBox1 = true;
        System.out.println(myBox1);

        char myBox1 = 'A';
        Character myBox2 = 'A';
        String myBox3 = "A";

        byte myBox4 = 1;
        Byte myBox5 = 1;

        short myBox6 = 2;
        int myBox7 = 10;
        long myBox8 = 13L;

        float myBox9 = 14.5f;
        double myBox10 = 1.5;



    }

}
